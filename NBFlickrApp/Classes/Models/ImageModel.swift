//
//  ImageModel.swift
//  NBFlickrApp
//
//  Created by Nikita Belosludcev on 30/05/2017.
//  Copyright © 2017 Nikta Belosludtcev. All rights reserved.
//

import UIKit

struct ImageModel {
    var id: String
    var owner: String
    var secret: String
    var server: String
    var farm: String
    var title: String
    
    init(id: String, owner: String, secret: String, server: String, farm: String, title: String) {
        self.id = id
        self.owner = owner
        self.secret = secret
        self.server = server
        self.farm = farm
        self.title = title
    }
    
}
