//
//  StringFromClass.swift
//  challengechatiosapp
//
//  Created by Nikita Belosludcev on 25/05/2017.
//  Copyright © 2017 challengechat. All rights reserved.
//

import Foundation

public extension NSObject{
    public class var nameOfClass: String{
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
}
