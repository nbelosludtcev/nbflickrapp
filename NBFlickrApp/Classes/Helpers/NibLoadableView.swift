//
//  NibLoadableView.swift
//  challengechatiosapp
//
//  Created by Nikita Belosludcev on 09/05/2017.
//  Copyright © 2017 challengechat. All rights reserved.
//

import Foundation
import UIKit

protocol NibLoadableView: class {
    static var nibName: String { get }
}

extension NibLoadableView where Self: UIView {
    static var nibName: String {
        return self.nameOfClass
    }
}
