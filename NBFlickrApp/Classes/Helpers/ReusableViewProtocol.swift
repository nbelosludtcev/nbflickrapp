//
//  ReusableViewProtocol.swift
//  challengechatiosapp
//
//  Created by Nikita Belosludcev on 09/05/2017.
//  Copyright © 2017 challengechat. All rights reserved.
//

import Foundation
import UIKit

protocol ReusableView: class {
    static var defaultReuseIdentifier: String { get }
}

extension ReusableView where Self: UIView {
    static var defaultReuseIdentifier: String {
        return self.nameOfClass
    }
}

extension UITableViewCell: ReusableView {}
extension UICollectionViewCell: ReusableView {}
