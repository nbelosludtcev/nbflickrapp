//
//  Coordinator.swift
//  NBFlickrApp
//
//  Created by Nikita Belosludcev on 30/05/2017.
//  Copyright © 2017 Nikta Belosludtcev. All rights reserved.
//

import Foundation
import UIKit

class ImagesSearchCoordinator {
    
    func createViewController() -> ImagesSearchVC {
        
        let searchImagesService = ImagesSearchServiceFlickrAPIImpl()
        let imageSerachViewModel = ImagesSearchViewModelImpl(withImageSearchService: searchImagesService)
        
        let imageSerachVC = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as! ImagesSearchVC
        
        imageSerachVC.viewModel = imageSerachViewModel
        imageSerachViewModel.viewDelegate = imageSerachVC
        
        return imageSerachVC
    }
    
}
