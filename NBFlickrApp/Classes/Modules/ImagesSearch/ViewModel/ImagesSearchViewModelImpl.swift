//
//  ImagesSearchViewModelImpl.swift
//  NBFlickrApp
//
//  Created by Nikita Belosludcev on 30/05/2017.
//  Copyright © 2017 Nikta Belosludtcev. All rights reserved.
//

import Foundation

final class ImagesSearchViewModelImpl: ImagesSearchViewModel {
    
    var imageSearchService: ImagesSearchService
    
    var viewDelegate: ImagesSearchViewModelDelegate?
    
    var errorMessage: String?
    var indicatorLoading: Bool = false
    
    private var images: [ImageModel]?
    var imagesViewModels: [ImageViewModel]?
    
    init(withImageSearchService imageSearchService: ImagesSearchService) {
        self.imageSearchService = imageSearchService
    }
    
    func search(withQuery query: String) {
        
        errorMessage = nil
        imagesViewModels = nil
        indicatorLoading = true
        
        self.viewDelegate?.queryResultsDidUpdate(self)
        
        guard query.characters.count > 0 else {
            indicatorLoading = false
            return
        }
        
        imageSearchService.fetchImages(withQuery: query, onSuccess: { [weak self] imagesModels in
            if let strongSelf = self {
                strongSelf.indicatorLoading = false
                strongSelf.images = imagesModels
                var imagesViewModelsGenerated: [ImageViewModel] = []
                
                for imageModel in imagesModels{
                    let imageURL = URL(string: "https://farm\(imageModel.farm).staticflickr.com/\(imageModel.server)/\(imageModel.id)_\(imageModel.secret).jpg")!
                    
                    let imageViewModel = ImageViewModel(imageUrl: imageURL, imageTitle: imageModel.title)
                    imagesViewModelsGenerated.append(imageViewModel)
                }
                strongSelf.imagesViewModels = imagesViewModelsGenerated
                
                strongSelf.viewDelegate?.queryResultsDidUpdate(strongSelf)
            }
            }, onError: { [weak self] errorMessage in
                if let strongSelf = self {
                    strongSelf.indicatorLoading = false
                    strongSelf.errorMessage = errorMessage
                    strongSelf.viewDelegate?.querySearchOnError(strongSelf)
                }
        })
    }
    
}
