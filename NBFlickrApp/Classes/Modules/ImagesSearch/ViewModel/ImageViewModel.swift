//
//  ImageViewModel.swift
//  NBFlickrApp
//
//  Created by Nikita Belosludcev on 30/05/2017.
//  Copyright © 2017 Nikta Belosludtcev. All rights reserved.
//

import Foundation

struct ImageViewModel {
    
    var imageUrl: URL?
    var imageTitle: String?
    
    init(imageUrl: URL, imageTitle: String) {
        self.imageUrl = imageUrl
        self.imageTitle = imageTitle
    }
}
