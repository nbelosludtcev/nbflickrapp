//
//  ImagesSearchViewModel.swift
//  NBFlickrApp
//
//  Created by Nikita Belosludcev on 30/05/2017.
//  Copyright © 2017 Nikta Belosludtcev. All rights reserved.
//

import Foundation

protocol ImagesSearchViewModelDelegate {
    func querySearchOnError(_ viewModel: ImagesSearchViewModel)
    func queryResultsDidUpdate(_ viewModel: ImagesSearchViewModel)
}

protocol ImagesSearchViewModel {
    
    var viewDelegate: ImagesSearchViewModelDelegate? { get set }
    
    var errorMessage: String? {get}
    var imagesViewModels: [ImageViewModel]? {get}
    
    var indicatorLoading: Bool {get}
    
    func search(withQuery query: String)
}
