//
//  ImagesSearchVC.swift
//  NBFlickrApp
//
//  Created by Nikita Belosludcev on 30/05/2017.
//    Copyright © 2017 Nikta Belosludtcev. All rights reserved.
//

import Foundation
import UIKit
import Typist
import SKPhotoBrowser

class ImagesSearchVC: UIViewController {
    
    var viewModel: ImagesSearchViewModel?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    var activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
    let keyboard = Typist.shared
    
    //MARK: Lifecycle
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(ImageCollectionViewCell.self)
        configureKeyboard()
        setupActivityIndicator()
        searchBar.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    
    //MARK: Private
    private func setupActivityIndicator() {
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = .gray
        let barButtonItem = UIBarButtonItem(customView: activityIndicator)
        navigationItem.rightBarButtonItem = barButtonItem;
    }
    
    private func configureKeyboard() {
        
        keyboard
            .on(event: .didShow) { [weak self]  (options) in
                self?.collectionView.contentInset = UIEdgeInsetsMake(0, 0, options.endFrame.height, 0)
                //print("New Keyboard Frame is \(options.endFrame).")
            }
            .on(event: .willHide) { [weak self] (options) in
                DispatchQueue.main.async {
                    UIView.animate(withDuration: options.animationDuration, animations: {
                        self?.collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
                        self?.collectionView.layoutIfNeeded()
                    })
                }
            }
            .start()
        
    }
    
}

//MARK: Collection view

extension ImagesSearchVC: UICollectionViewDataSource {
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.imagesViewModels?.count ?? 0
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let imageCell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCollectionViewCell.defaultReuseIdentifier, for: indexPath) as? ImageCollectionViewCell {
            imageCell.imageViewModel = self.viewModel?.imagesViewModels?[indexPath.row]
            return imageCell
        }
        return UICollectionViewCell()
    }
}

extension ImagesSearchVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let imageSizeWidth = UIScreen.main.bounds.size.width / 4.0;
        return CGSize(width: imageSizeWidth, height: imageSizeWidth)
    }
}

extension ImagesSearchVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        
        if let viewModel = self.viewModel, let imagesViewModels = viewModel.imagesViewModels, imagesViewModels.count > indexPath.row {
            
            var images = [SKPhoto]()
            for imageViewModel in imagesViewModels {
                if let imageUrl = imageViewModel.imageUrl {
                    let photo = SKPhoto.photoWithImageURL(imageUrl.absoluteString)
                    photo.caption = imageViewModel.imageTitle
                    photo.shouldCachePhotoURLImage = true
                    images.append(photo)
                }
            }
            
            if indexPath.row < images.count {
                let browser = SKPhotoBrowser(photos: images)
                browser.initializePageIndex(indexPath.row)
                self.present(browser, animated: true, completion: {})
            } else {
                let alertVC = UIAlertController(title: "Error", message: "Can't load image url", preferredStyle: .alert)
                alertVC.addAction(UIAlertAction(title: "Ок", style: .cancel, handler: nil))
                self.present(alertVC, animated: true, completion: nil)
            }
        }
    }
}

//MARK: UIScrollViewDelegate

extension ImagesSearchVC: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.searchBar.resignFirstResponder()
    }
}

//MARK: Search bar

extension ImagesSearchVC: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let viewModel = self.viewModel {
            activityIndicator.startAnimating()
            viewModel.search(withQuery: searchText)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

//MARK: View model delegate

extension ImagesSearchVC: ImagesSearchViewModelDelegate {
    func querySearchOnError(_ viewModel: ImagesSearchViewModel) {
        
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.collectionView.reloadData()
            if let errorMessage = viewModel.errorMessage {
                let alertVC = UIAlertController(title: "Error", message: errorMessage, preferredStyle: .alert)
                alertVC.addAction(UIAlertAction(title: "Ок", style: .cancel, handler: nil))
                self.present(alertVC, animated: true, completion: nil)
            }
        }
    }
    
    func queryResultsDidUpdate(_ viewModel: ImagesSearchViewModel) {
        DispatchQueue.main.async {
            if !viewModel.indicatorLoading {
                self.activityIndicator.stopAnimating()
            }
            
            self.collectionView.reloadData()
        }
    }
}
