//
//  ImagesSearchServiceFakeImpl.swift
//  NBFlickrApp
//
//  Created by Nikita Belosludcev on 30/05/2017.
//  Copyright © 2017 Nikta Belosludtcev. All rights reserved.
//

import UIKit

class ImagesSearchServiceFakeImpl: ImagesSearchService {
    
    func fetchImages(withQuery query: String, onSuccess success: @escaping ([ImageModel])->Void, onError error: @escaping (String)->Void) {
        //success([ImageModel(),ImageModel()])
        error("Ошибка")
    }
    
}
