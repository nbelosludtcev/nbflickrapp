//
//  ImagesSearchServiceFlickrAPIImpl.swift
//  NBFlickrApp
//
//  Created by Nikita Belosludcev on 30/05/2017.
//  Copyright © 2017 Nikta Belosludtcev. All rights reserved.
//

import UIKit
import AEXML

class ImagesSearchServiceFlickrAPIImpl: NSObject, ImagesSearchService {
    
    let defaultSession = URLSession(configuration: URLSessionConfiguration.default)
    
    var dataTask: URLSessionDataTask?
    
    let FlickARIKey = "53e629dd511454f0fdaec1f0d9ff8cec"
    
    func fetchImages(withQuery query: String, onSuccess success: @escaping ([ImageModel])->Void, onError errorBlock: @escaping (String)->Void) {
        
        if query.characters.count > 0 {
            if dataTask != nil {
                dataTask?.cancel()
            }
            let searchTerm = query.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            if let  searchTerm = searchTerm {
                let url = URL(string: "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=53e629dd511454f0fdaec1f0d9ff8cec&text=\(searchTerm)&format=rest")
                
                dataTask = defaultSession.dataTask(with:url!) {
                    data, response, error in
                    if let error = error {
                        let nsError = error as NSError
                        //task cancelation code
                        if nsError.code != -999 {
                            errorBlock(nsError.localizedDescription)
                        }
                    } else if let httpResponse = response as? HTTPURLResponse {
                        if httpResponse.statusCode == 200 {
                            if let data = data {
                                
                                do {
                                    let xmlDoc = try AEXMLDocument(xml: data)
                                    var imagesModels: [ImageModel] = []
                                    for child in xmlDoc.root["photos"].children {
                                        
                                        if let id = child.attributes["id"],
                                            let owner = child.attributes["owner"],
                                            let secret = child.attributes["secret"],
                                            let server = child.attributes["server"],
                                            let farm = child.attributes["farm"],
                                            let title = child.attributes["title"] {
                                            let imageModel = ImageModel(id: id,
                                                                        owner: owner,
                                                                        secret: secret,
                                                                        server: server,
                                                                        farm: farm,
                                                                        title: title)
                                            imagesModels.append(imageModel)
                                        }
                                    }
                                    success(imagesModels)
                                    
                                } catch {
                                    errorBlock("Can't parse responce")
                                }
                            }
                        }
                    }
                }
                dataTask?.resume()
            } else {
                errorBlock("Wrong search query")
            }
            
            
        } else {
            errorBlock("Empty search query")
        }
        
    }
    
}
