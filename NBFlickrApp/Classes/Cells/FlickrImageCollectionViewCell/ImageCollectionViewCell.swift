//
//  ImageCollectionViewCell.swift
//  NBFlickrApp
//
//  Created by Nikita Belosludcev on 30/05/2017.
//  Copyright © 2017 Nikta Belosludtcev. All rights reserved.
//

import UIKit
import Kingfisher

class ImageCollectionViewCell: UICollectionViewCell, NibLoadableView {

    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    public var imageViewModel: ImageViewModel? {
        didSet {
            activityIndicator.startAnimating()
            if let imageViewModel = imageViewModel, let imageUrl = imageViewModel.imageUrl {
                image.kf.setImage(with: imageUrl,completionHandler: { [weak self] image, error, cacheType, url in
                    self?.activityIndicator.stopAnimating()
                })
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
